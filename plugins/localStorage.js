import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  // window.onNuxtReady(() => {
    createPersistedState({
        paths: ['authLocalStore','productStore', 'tablesStore', ,'tempOrdersStore','ordersStore']
    })(store)
  // })
}

// var pcat = ['food']
// var ps = [
//   {
//     pname : 'ddd',
//     price : 100 
//   },
//   {
//     pname : 'fff',
//     price : 200
//   }
// ]
// var data = []
// pcat.map((each, index) => each === 'food' ? data = ps : null)
// console.log(data.pname)